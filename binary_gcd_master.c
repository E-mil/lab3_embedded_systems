#include <stdio.h>
#include <stdint.h>

#include <mb_interface.h>

unsigned getnum();
unsigned gcd(uint32_t a, uint32_t b);

int main(void) {
    uint32_t N = getnum();
    uint32_t i, middle, current_gcd;
    uint32_t nums[N];
    for (i = 0; i < N; i++) {
        nums[i] = getnum();
    }
    middle = N >> 1;
    putfsl(middle, 0);
    for (i = 0; i < middle; i++) {
        putfsl(nums[i], 0);
    }
    current_gcd = nums[middle];
    for (i = middle+1; i < N; i++) {
        current_gcd = gcd(current_gcd, nums[i]);
    }
    getfsl(i, 0);
    current_gcd = gcd(current_gcd, i);
    xil_printf("gcd = %d\n", current_gcd);
    return 0;
}

unsigned gcd(uint32_t a, uint32_t b) {
    uint32_t evens = 0;
    if (a == 0) {
        return b;
    }
    if (b == 0) {
        return a;
    }
    while (a != b) {
        if (a & 1) { // a is odd
            if (b & 1) { // b is odd
                if (a < b) {
                    b = (b - a) >> 1;
                } else {
                    a = (a - b) >> 1;
                }
            } else { // b is even
                b = b >> 1;
            }
        } else { // a is even
            a = a >> 1;
            if (~b & 1) { // b is even
                b = b >> 1;
                evens += 1;
            }
        }
    }
    return a << evens;
}